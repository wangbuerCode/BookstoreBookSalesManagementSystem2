package com.faded.bookstore.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;
import com.faded.bookstore.service.BookService;
import com.faded.bookstore.service.impl.BookServiceImpl;
import com.faded.bookstore.utils.WebUtils;

/**
 * 后台管理图书的Servlet
 */
public class BookManagerServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	private BookService bookService = new BookServiceImpl();

	// 获取带分页的图书
	protected void getPageBooks(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取页码
		String pageNo = request.getParameter("pageNo");
		//获取请求地址
		String path = WebUtils.getPath(request);
		// 调用bookService的方法获取带分页的图书信息
		Page<Book> pageBooks = bookService.getPageBooks(pageNo);
		//将path设置到pageBooks对象中
		pageBooks.setPath(path);
		// 将pageBooks对象放到request域中
		request.setAttribute("page", pageBooks);
		// 转发到显示所有图书的页面
		request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request, response);
	};

	// 添加或更新图书的方法
	protected void saveOrUpdateBook(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取图书信息
		String bookId = request.getParameter("bookId");

		String title = request.getParameter("book_name");
		String author = request.getParameter("book_author");
		String price = request.getParameter("book_price");
		String sales = request.getParameter("book_sales");
		String stock = request.getParameter("book_stock");
		// 判断用户在添加图书还是在更新图书
		if ("".equals(bookId)) {
			// 证明在添加图书
			// 封装Book对象
			Book book = new Book(null, title, author, Double.parseDouble(price), Integer.parseInt(sales),
					Integer.parseInt(stock));
			// 调用bookService的方法保存图书
			bookService.saveBook(book);
		} else {
			// 证明在更新图书
			// 封装Book对象
			Book book = new Book(Integer.parseInt(bookId), title, author, Double.parseDouble(price),
					Integer.parseInt(sales), Integer.parseInt(stock));
			// 调用bookService的方法更新图书信息
			bookService.updateBook(book);
		}
		// 方式二：重定向到显示所有图书的方法
		response.sendRedirect(request.getContextPath() + "/BookManagerServlet?way=getPageBooks");
	}

	// 根据图书的id更新图书信息
	// protected void updateBook(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException {
	// // 获取图书信息
	// String bookId = request.getParameter("bookId");
	//
	// String title = request.getParameter("book_name");
	// String author = request.getParameter("book_author");
	// String price = request.getParameter("book_price");
	// String sales = request.getParameter("book_sales");
	// String stock = request.getParameter("book_stock");
	// // 封装Book对象
	// Book book = new Book(Integer.parseInt(bookId), title, author,
	// Double.parseDouble(price),
	// Integer.parseInt(sales), Integer.parseInt(stock));
	// // 调用bookService的方法更新图书信息
	// bookService.updateBook(book);
	// // 方式二：重定向到显示所有图书的方法
	// response.sendRedirect(request.getContextPath() +
	// "/BookManagerServlet?way=getBooks");
	// }

	// 根据图书的id获取图书信息
	protected void getBookById(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取图书的id
		String bookId = request.getParameter("bookId");
		// 调用bookService的方法获取图书信息
		Book bookById = bookService.getBookById(bookId);
		// 将要修改的图书放到request域中
		request.setAttribute("book", bookById);
		// 转发到修改图书的页面
		request.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(request, response);
	}

	// 根据图书的id删除图书的方法
	protected void deleteBookById(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取图书的id
		String bookId = request.getParameter("bookId");
		// 调用bookService的方法删除该图书
		bookService.deleteBookById(bookId);
		// 方式二：重定向到显示所有图书的方法
		response.sendRedirect(request.getContextPath() + "/BookManagerServlet?way=getPageBooks");
	}

	// 添加图书的方法
	// protected void addBook(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException {
	// // 获取用户输入的图书的信息
	// String title = request.getParameter("book_name");
	// String author = request.getParameter("book_author");
	// String price = request.getParameter("book_price");
	// String sales = request.getParameter("book_sales");
	// String stock = request.getParameter("book_stock");
	// // 封装Book对象
	// Book book = new Book(null, title, author, Double.parseDouble(price),
	// Integer.parseInt(sales),
	// Integer.parseInt(stock));
	// // 调用bookService的方法保存图书
	// bookService.saveBook(book);
	// // 方式一：直接调用getBooks方法再次获取一下数据库中的图书
	// // getBooks(request, response);
	// // 方式二：重定向到显示所有图书的方法
	// response.sendRedirect(request.getContextPath() +
	// "/BookManagerServlet?way=getBooks");
	// }

	// 获取所有图书的方法
//	protected void getBooks(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// 调用bookService的方法获取所有的图书
//		List<Book> books = bookService.getBooks();
//		// 将books放到request域中
//		request.setAttribute("books", books);
//		// 转发到显示所有图书的页面
//		request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request, response);
//	}

}
