package com.faded.bookstore.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;
import com.faded.bookstore.service.BookService;
import com.faded.bookstore.service.impl.BookServiceImpl;
import com.faded.bookstore.utils.WebUtils;

/**
 * 前台管理图书的Servlet
 */
public class BookClientServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	private BookService bookService = new BookServiceImpl();

	// 获取带价格范围和分页的图书
	protected void getPageBooksByPrice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取页码
		String pageNo = request.getParameter("pageNo");
		// 获取用户输入的价格
		String minPrice = request.getParameter("min");
		String maxPrice = request.getParameter("max");
		System.out.println(minPrice);
		System.out.println(maxPrice);
		// 调用bookService的方法获取带价格范围和分页的图书信息
		Page<Book> pageBooksByPrice = bookService.getPageBooksByPrice(pageNo, minPrice, maxPrice);
		// 获取请求地址
		String path = WebUtils.getPath(request);
		// 将path设置到pageBooksByPrice中
		pageBooksByPrice.setPath(path);
		// 将pageBooksByPrice放到request域中
		request.setAttribute("page", pageBooksByPrice);
		// 转发到显示所有图书的页面
		request.getRequestDispatcher("/pages/client/books.jsp").forward(request, response);
	}

	// 获取带分页的图书
	protected void getPageBooks(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取页码
		String pageNo = request.getParameter("pageNo");
		// 获取请求地址
		String path = WebUtils.getPath(request);
		// 调用bookService的方法获取带分页的图书信息
		Page<Book> pageBooks = bookService.getPageBooks(pageNo);
		// 将path设置到pageBooks对象中
		pageBooks.setPath(path);
		// 将pageBooks对象放到request域中
		request.setAttribute("page", pageBooks);
		// 转发到显示所有图书的页面
		request.getRequestDispatcher("/pages/client/books.jsp").forward(request, response);
	};

	// 获取所有图书的方法
	// protected void getBooks(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException {
	// // 调用bookService的方法获取所有的图书
	// List<Book> books = bookService.getBooks();
	// // 将所有的图书放到request域中
	// request.setAttribute("books", books);
	// // 转发到显示图书的页面
	// request.getRequestDispatcher("/pages/client/books.jsp").forward(request,
	// response);
	// }

}
