package com.faded.bookstore.dao;

import java.util.List;

import com.faded.bookstore.beans.Order;
import com.faded.bookstore.beans.OrderItem;

public interface OrderDao {

	/**
	 * 保存订单的方法
	 * 
	 * @param order
	 */
	public void saveOrder(Order order);

	/**
	 * 获取所用订单的方法
	 * 
	 * @return
	 */
	public List<Order> getOrders();

	/**
	 * 获取我的订单的方法
	 * 
	 * @param userId
	 * @return
	 */
	public List<Order> getMyOrders(int userId);

	/**
	 * 更新订单的状态的方法
	 * 
	 * @param orderId
	 * @param state
	 */
	public void updateOrderState(String orderId, int state);

}
