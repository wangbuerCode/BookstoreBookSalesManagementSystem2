package com.faded.bookstore.dao.impl;

import java.util.List;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;
import com.faded.bookstore.dao.BaseDao;
import com.faded.bookstore.dao.BookDao;

public class BookDaoImpl extends BaseDao<Book> implements BookDao {

	@Override
	public List<Book> getBooks() {
		// 写sql语句
		String sql = "select id,title,author,price,sales,stock,img_path imgPath from books";
		// 调用BaseDao的getBeanList方法获取一个集合
		List<Book> beanList = getBeanList(sql);
		return beanList;
	}

	@Override
	public void addBook(Book book) {
		// 写sql语句
		String sql = "insert into books(title,author,price,sales,stock,img_path) values(?,?,?,?,?,?)";
		update(sql, book.getTitle(), book.getAuthor(), book.getPrice(), book.getSales(), book.getStock(),
				book.getImgPath());
	}

	@Override
	public void deleteBookById(String bookId) {
		// 写sql语句
		String sql = "delete from books where id = ?";
		update(sql, bookId);
	}

	@Override
	public Book getBookById(String bookId) {
		// 写sql语句
		String sql = "select id,title,author,price,sales,stock,img_path imgPath from books where id = ?";
		// 调用BaseDao的getBean方法
		Book bean = getBean(sql, bookId);
		return bean;
	}

	@Override
	public void updateBook(Book book) {
		// 写sql语句
		String sql = "update books set title = ? , author = ? , price = ? , sales = ? , stock = ?  where id = ?";
		update(sql, book.getTitle(), book.getAuthor(), book.getPrice(), book.getSales(), book.getStock(), book.getId());
	}

	@Override
	public Page<Book> getPageBooks(Page<Book> page) {
		// 获取数据库中图书的总记录数
		String sql = "select count(*) from books";
		long totalRecord = (Long) getSingelValue(sql);
		// 将总记录数设置到page对象中
		page.setTotalRecord((int) totalRecord);

		// 获取当前页中带分页的list
		String sql2 = "select id,title,author,price,sales,stock,img_path imgPath from books limit ?,?";
		List<Book> beanList = getBeanList(sql2, (page.getPageNo() - 1) * Page.PAGE_SIZE, Page.PAGE_SIZE);
		// 将带分页的集合设置到page对象中
		page.setList(beanList);
		return page;
	}

	@Override
	public Page<Book> getPageBooksByPrice(Page<Book> page, double minPrice, double maxPrice) {
		// 获取数据库中图书的总记录数
		String sql = "select count(*) from books where price between ? and ?";
		long totalRecord = (Long) getSingelValue(sql,minPrice,maxPrice);
		// 将总记录数设置到page对象中
		page.setTotalRecord((int) totalRecord);

		// 获取当前页中带分页的list
		String sql2 = "select id,title,author,price,sales,stock,img_path imgPath from books where price between ? and ? limit ?,?";
		List<Book> beanList = getBeanList(sql2,minPrice,maxPrice,(page.getPageNo() - 1) * Page.PAGE_SIZE, Page.PAGE_SIZE);
		// 将带分页的集合设置到page对象中
		page.setList(beanList);
		return page;
	}

	@Override
	public void batchUpdateSalesAndStock(Object[][] params) {
		//写sql语句
		String sql = "update books set sales = ? , stock = ? where id = ?";
		batchUpdate(sql, params);
	}

}
