package com.faded.bookstore.dao;

import java.util.List;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;

public interface BookDao {

	/**
	 * 获取所有图书的方法
	 * 
	 * @return
	 */
	public List<Book> getBooks();

	/**
	 * 添加图书的方法
	 * 
	 * @param book
	 */
	public void addBook(Book book);

	/**
	 * 根据图书的id删除图书的方法
	 * 
	 * @param bookId
	 */
	public void deleteBookById(String bookId);

	/**
	 * 根据图书的id获取图书信息
	 * 
	 * @param bookId
	 * @return
	 */
	public Book getBookById(String bookId);

	/**
	 * 更新图书信息的方法
	 * 
	 * @param book
	 */
	public void updateBook(Book book);

	/**
	 * 获取带分页的所有图书信息
	 * 
	 * @param page
	 *            传入的page对象是一个带页码的page对象
	 * @return 返回的page对象是包含所有属性的page对象
	 */
	public Page<Book> getPageBooks(Page<Book> page);

	/**
	 * 根据价格范围获取带分页的图书信息
	 * 
	 * @param page
	 * @param minPrice
	 * @param maxPrice
	 * @return
	 */
	public Page<Book> getPageBooksByPrice(Page<Book> page, double minPrice, double maxPrice);

	/**
	 * 批量更新图书的库存和销量
	 * 
	 * @param params
	 */
	public void batchUpdateSalesAndStock(Object[][] params);
}
