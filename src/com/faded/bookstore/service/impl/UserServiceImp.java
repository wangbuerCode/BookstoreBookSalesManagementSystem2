package com.faded.bookstore.service.impl;

import com.faded.bookstore.beans.User;
import com.faded.bookstore.dao.UserDao;
import com.faded.bookstore.dao.impl.UserDaoImpl;
import com.faded.bookstore.service.UserService;

public class UserServiceImp implements UserService {

	UserDao userDao = new UserDaoImpl();
	@Override
	public User login(User user) {
		return userDao.getUser(user);
	}

	@Override
	public boolean regist(User user) {
		return userDao.checkUserName(user);
	}

	@Override
	public void saveUser(User user) {
		userDao.saveUser(user);
	}

}
