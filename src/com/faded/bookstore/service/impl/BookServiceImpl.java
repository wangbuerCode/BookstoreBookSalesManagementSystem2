package com.faded.bookstore.service.impl;

import java.util.List;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;
import com.faded.bookstore.dao.BookDao;
import com.faded.bookstore.dao.impl.BookDaoImpl;
import com.faded.bookstore.service.BookService;

public class BookServiceImpl implements BookService {

	BookDao bookDao = new BookDaoImpl();

	@Override
	public List<Book> getBooks() {
		return bookDao.getBooks();
	}

	@Override
	public void saveBook(Book book) {
		bookDao.addBook(book);
	}

	@Override
	public void deleteBookById(String bookId) {
		bookDao.deleteBookById(bookId);
	}

	@Override
	public Book getBookById(String bookId) {
		return bookDao.getBookById(bookId);
	}

	@Override
	public void updateBook(Book book) {
		bookDao.updateBook(book);
	}

	@Override
	public Page<Book> getPageBooks(String pageNo) {
		// 创建Page对象
		Page<Book> page = new Page<Book>();
		// 设置一个默认的页码
		int defaultPageNo = 1;
		// 将页码转换为int类型
		try {
			defaultPageNo = Integer.parseInt(pageNo);
		} catch (Exception e) {
		}
		// 将页码设置到page对象中
		page.setPageNo(defaultPageNo);
		return bookDao.getPageBooks(page);
	}

	@Override
	public Page<Book> getPageBooksByPrice(String pageNo, String minPrice, String maxPrice) {
		// 创建Page对象
		Page<Book> page = new Page<Book>();
		// 设置一个默认的页码
		int defaultPageNo = 1;
		
		//设置两个默认的价格
		double defaultMinPrice = 0;
		double defaultMaxPrice = Double.MAX_VALUE;
		// 将页码转换为int类型
		try {
			defaultPageNo = Integer.parseInt(pageNo);
		} catch (Exception e) {}
		//对价格范围进行强转，要单独出来异常
		try {
			defaultMinPrice = Double.parseDouble(minPrice);
		} catch (Exception e) {}
		try {
			defaultMaxPrice = Double.parseDouble(maxPrice);
		} catch (Exception e) {}
		// 将页码设置到page对象中
		page.setPageNo(defaultPageNo);
		return bookDao.getPageBooksByPrice(page, defaultMinPrice, defaultMaxPrice);
	}

}
