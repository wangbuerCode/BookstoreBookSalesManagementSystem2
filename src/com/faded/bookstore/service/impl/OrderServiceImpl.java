package com.faded.bookstore.service.impl;

import java.util.Date;
import java.util.List;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Cart;
import com.faded.bookstore.beans.CartItem;
import com.faded.bookstore.beans.Order;
import com.faded.bookstore.beans.OrderItem;
import com.faded.bookstore.beans.User;
import com.faded.bookstore.dao.BookDao;
import com.faded.bookstore.dao.OrderDao;
import com.faded.bookstore.dao.OrderItemDao;
import com.faded.bookstore.dao.impl.BookDaoImpl;
import com.faded.bookstore.dao.impl.OrderDaoImpl;
import com.faded.bookstore.dao.impl.OrderItemDaoImpl;
import com.faded.bookstore.service.OrderService;

public class OrderServiceImpl implements OrderService {

	OrderDao orderDao = new OrderDaoImpl();
	OrderItemDao orderItemDao = new OrderItemDaoImpl();
	BookDao bookDao = new BookDaoImpl();
	
	@Override
	public String createOrder(User user , Cart cart) {
		//生成订单号
		String orderId = System.currentTimeMillis()+""+user.getId();
		//获取购物车中的图书的总数量
		int totalCount = cart.getTotalCount();
		//获取购物车中图书的总金额
		double totalAmount = cart.getTotalAmount();
		//创建Order对象
		Order order = new Order(orderId, new Date(), totalCount, totalAmount, 0, user.getId());
		//将订单保存到数据库中
		orderDao.saveOrder(order);
		
		//获取购物车中所有的购物项
		List<CartItem> cartItems = cart.getCartItems();
		//创建两个二维数组
		Object[][] itemParams = new Object[cartItems.size()][];
		Object[][] bookParams = new Object[cartItems.size()][];
		//遍历得到每一个购物项
		for(int i = 0 ; i < cartItems.size() ; i++){
			CartItem cartItem = cartItems.get(i);
			//获取购物项中的图书的数量
			int count = cartItem.getCount();
			//获取购物项中的金额小计
			double amount = cartItem.getAmount();
			//获取购物项中的图书
			Book book = cartItem.getBook();
			//获取书名
			String title = book.getTitle();
			//获取作者
			String author = book.getAuthor();
			//获取图书的价格
			double price = book.getPrice();
			//获取图书的封面
			String imgPath = book.getImgPath();
			//封装OrderItem对象
//			OrderItem orderItem = new OrderItem(null, count, amount, title, author, price, imgPath, orderId);
			//将订单项保存到数据库中
//			orderItemDao.saveOrderItem(orderItem);
			//insert into order_items(count,amount,title,author,price,img_path,order_id) values(?,?,?,?,?,?,?)
			itemParams[i] = new Object[]{count, amount, title, author, price, imgPath, orderId};
			
			//获取图书的库存和销量
			Integer sales = book.getSales();
			Integer stock = book.getStock();
			//设置该图书新的库存和销量
//			book.setSales(sales + count);
//			book.setStock(stock - count);
			//更新图书的库存和销量
//			bookDao.updateBook(book);
			//update books set sales = ? , stock = ? where id = ?
			bookParams[i] = new Object[]{sales + count , stock - count , book.getId()};
		}
		
		//批量插入订单项
		orderItemDao.batchInsertOrderItems(itemParams);
		//批量更新图书的库存和销量
		bookDao.batchUpdateSalesAndStock(bookParams);
		
		//结账之后需要清空购物车
		cart.clearCart();
		return orderId;
	}

	@Override
	public List<Order> getOrders() {
		return orderDao.getOrders();
	}

	@Override
	public List<OrderItem> getOrderItemsByOrderId(String orderId) {
		return orderItemDao.getOrderItemsByOrderId(orderId);
	}

	@Override
	public List<Order> getMyOrders(int userId) {
		return orderDao.getMyOrders(userId);
	}

	@Override
	public void updateOrderState(String orderId, int state) {
		orderDao.updateOrderState(orderId, state);
	}

}
