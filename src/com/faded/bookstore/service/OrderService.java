package com.faded.bookstore.service;

import java.util.List;

import com.faded.bookstore.beans.Cart;
import com.faded.bookstore.beans.Order;
import com.faded.bookstore.beans.OrderItem;
import com.faded.bookstore.beans.User;

public interface OrderService {

	/**
	 * 去结账生成订单的方法
	 * 
	 * @param user
	 * @param cart
	 * @return
	 */
	public String createOrder(User user, Cart cart);

	/**
	 * 获取所用订单的方法
	 * 
	 * @return
	 */
	public List<Order> getOrders();

	/**
	 * 根据订单号获取对应的订单项
	 * 
	 * @param orderId
	 * @return
	 */
	public List<OrderItem> getOrderItemsByOrderId(String orderId);

	/**
	 * 获取我的订单
	 * 
	 * @param userId
	 * @return
	 */
	public List<Order> getMyOrders(int userId);

	/**
	 * 发货和确认收货的方法
	 * 
	 * @param orderId
	 * @param state
	 */
	public void updateOrderState(String orderId, int state);
}
