package com.faded.bookstore.service;

import java.util.List;

import com.faded.bookstore.beans.Book;
import com.faded.bookstore.beans.Page;

public interface BookService {

	/**
	 * 获取所有的图书
	 * 
	 * @return
	 */
	public List<Book> getBooks();

	/**
	 * 保存图书的方法
	 * 
	 * @param book
	 */
	public void saveBook(Book book);

	/**
	 * 根据图书的id删除图书的方法
	 * 
	 * @param bookId
	 */
	public void deleteBookById(String bookId);

	/**
	 * 根据图书的id获取图书信息
	 * 
	 * @param bookId
	 * @return
	 */
	public Book getBookById(String bookId);

	/**
	 * 更新图书信息的方法
	 * 
	 * @param book
	 */
	public void updateBook(Book book);

	/**
	 * 获取带分页的图书信息
	 * 
	 * @param pageNo
	 * @return
	 */
	public Page<Book> getPageBooks(String pageNo);

	/**
	 * 根据价格范围获取带分页的图书信息
	 * 
	 * @param page
	 * @param minPrice
	 * @param maxPrice
	 * @return
	 */
	public Page<Book> getPageBooksByPrice(String pageNo, String minPrice, String maxPrice);
}
