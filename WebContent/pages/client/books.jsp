<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>书城首页</title>
<%@ include file="/WEB-INF/include/base.jsp" %>
<script type="text/javascript">
	$(function(){
		//给添加购物车的按钮绑定单击事件
		$(".addBook").click(function(){
			//获取图书的id
			var bookId = $(this).attr("id");
			//发送请求
			location = "CartServlet?way=addBook&bookId="+bookId;
		});
	});
</script>
</head>
<body>
	
	<div id="header">
			<span class="wel_word">网上书城</span>
			<%@ include file="/WEB-INF/include/welcome.jsp" %>
	</div>
	
	<div id="main">
		<div id="book">
			<div class="book_cond">
			
			</div>
			<div style="text-align: center">
				<c:if test="${not empty cart.cartItems }">
					<span>您的购物车中有${cart.totalCount }件商品</span>
				</c:if>	
				<c:if test="${empty cart.cartItems }">
					<span>您的购物车空空如也</span>
				</c:if>	
					<c:if test="${not empty bookTitle }">
						<div>
							您刚刚将<span style="color: red">${bookTitle }</span>加入到了购物车中
							<c:remove var="bookTitle"/>
						</div>
					</c:if>	
				<div>
					<span style="color: red">${msg }</span>
					<c:remove var="msg"/>
				</div>	
			</div>
		<c:forEach items="${page.list }" var="book">
			<div class="b_list">
				<div class="img_div">
					<img class="book_img" alt="" src="${book.imgPath }" />
				</div>
				<div class="book_info">
					<div class="book_name">
						<span class="sp1">书名:</span>
						<span class="sp2">${book.title }</span>
					</div>
					<div class="book_author">
						<span class="sp1">作者:</span>
						<span class="sp2">${book.author }</span>
					</div>
					<div class="book_price">
						<span class="sp1">价格:</span>
						<span class="sp2">￥${book.price }</span>
					</div>
					<div class="book_sales">
						<span class="sp1">销量:</span>
						<span class="sp2">${book.sales }</span>
					</div>
					<div class="book_amount">
						<span class="sp1">库存:</span>
						<span class="sp2">${book.stock }</span>
					</div>
					<c:if test="${book.stock > 0 }">
						<div class="book_add">
							<button class="addBook" id="${book.id }">加入购物车</button>
						</div>
					</c:if>
					<c:if test="${book.stock == 0 }">
						<div class="book_add">
							<span style="color: red">小二拼命补货中</span>
						</div>
					</c:if>
				</div>
			</div>
			</c:forEach>	
		</div>
		
	<!-- 使用静态包含将页码包含进来 -->
	<%@ include file="/WEB-INF/include/page.jsp" %>
	
	</div>
	
	<div id="bottom">
		<span>
			书店图书管理系统
		</span>
	</div>
</body>
</html>