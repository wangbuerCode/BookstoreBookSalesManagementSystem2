<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>错误页面</title>
<%@ include file="/WEB-INF/include/base.jsp" %>
</head>
<body>
	
	<div id="header">
			<span class="wel_word">出错啦！</span>
			<%@ include file="/WEB-INF/include/welcome.jsp" %>
	</div>
	
	<div id="main">
		<h1>系统出现异常，请联系<a href="#" style="color: red">管理员</a></h1>
	</div>
	
	<div id="bottom">
		<span>
			书店图书管理系统
		</span>
	</div>
</body>
</html>