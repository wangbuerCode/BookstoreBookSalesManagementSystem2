<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>图书管理</title>
<%@ include file="/WEB-INF/include/base.jsp" %>
<script type="text/javascript">
	$(function(){
		//给所有删除图书的超链接绑定单击事件
		$(".deleteBook").click(function(){
			//获取图书的名字
// 			var bookName = $(this).parent().parent().children("td:first").text();
// 			var bookName = $(this).parents("tr").children("td:first").text();
			var bookName = $(this).attr("id");
			var flag = confirm("确定要删除【"+bookName+"】这本图书吗？");
// 			if(!flag){
// 				return false;
// 			}
			return flag;
		});
	});
</script>
</head>
<body>
	
	<div id="header">
			<span class="wel_word">图书管理系统</span>
			<%@ include file="/WEB-INF/include/header.jsp" %>
	</div>
	
	<div id="main">
	<c:if test="${empty page.list }">
		<h1>没有任何图书</h1>
	</c:if>	
	<c:if test="${not empty page.list }">
		<table>
			<tr>
				<td>名称</td>
				<td>价格</td>
				<td>作者</td>
				<td>销量</td>
				<td>库存</td>
				<td colspan="2">操作</td>
			</tr>	
		<c:forEach items="${page.list }" var="book">	
			<tr>
				<td>${book.title }</td>
				<td>${book.price }</td>
				<td>${book.author }</td>
				<td>${book.sales }</td>
				<td>${book.stock }</td>
				<td><a href="BookManagerServlet?way=getBookById&bookId=${book.id }">修改</a></td>
				<td><a id="${book.title }" class="deleteBook" href="BookManagerServlet?way=deleteBookById&bookId=${book.id }">删除</a></td>
			</tr>	
		</c:forEach>	
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="pages/manager/book_edit.jsp">添加图书</a></td>
			</tr>	
		</table>
	</c:if>	
	
	<div id="page_nav">
		<c:if test="${page.pageNo > 1 }">
			<a href="BookManagerServlet?way=getPageBooks">首页</a>
			<a href="BookManagerServlet?way=getPageBooks&pageNo=${page.prev }">上一页</a>
		</c:if>
		当前是${page.pageNo }页，共${page.totalPageNo }页，共${page.totalRecord }条记录
		<c:if test="${page.pageNo < page.totalPageNo }">
			<a href="BookManagerServlet?way=getPageBooks&pageNo=${page.next }">下一页</a>
			<a href="BookManagerServlet?way=getPageBooks&pageNo=${page.totalPageNo }">末页</a>
		</c:if>
		 到第<input value="${page.pageNo }" name="pn" id="pn_input"/>页
		<input type="button" value="确定" id="subBtn">
		<script type="text/javascript">
			$("#subBtn").click(function(){
				//获取输入的页面
				var pageNo = $("#pn_input").val();
				//发送请求
// 				window.location.href="BookManagerServlet?way=getPageBooks&pageNo="+pageNo;
// 				window.location="BookManagerServlet?way=getPageBooks&pageNo="+pageNo;
				location="${pageContext.request.contextPath}/BookManagerServlet?way=getPageBooks&pageNo="+pageNo;
			});
		</script>
	</div>
	
	</div>
	
	<div id="bottom">
		<span>
			书店图书管理系统
		</span>
	</div>
</body>
</html>